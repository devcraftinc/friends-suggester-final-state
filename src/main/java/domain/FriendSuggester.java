package domain;

import java.util.List;

public class FriendSuggester {
	public List<String> suggest(String user, List<Message> messages) {
		
		SuggestFriendsVisitor v = new SuggestFriendsVisitor(user);
		
		for (Message m : messages) {
			m.accept(v);
		}
		return v.getResult();
	}

}
