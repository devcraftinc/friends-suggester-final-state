package domain;

public interface MsgVisitor {

	void visit(TextMessage t);

	void visit(PhotoMessage p);

	void visit(AudioMessage a);

}