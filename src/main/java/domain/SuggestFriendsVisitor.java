package domain;

import java.util.ArrayList;
import java.util.List;

public class SuggestFriendsVisitor implements MsgVisitor {
	private String user;
	private List<String> result;

	public SuggestFriendsVisitor(String user) {
		this.user = user;
		this.result = new ArrayList<>();
	}

	public void visit(TextMessage t) {
		if (t.getText().contains(user)) {
			result.add(t.getSender());
			result.add(t.getReceiver());
		}
	}

	public void visit(PhotoMessage p) {
		if (p.getCaption().contains(user)) {
			result.add(p.getSender());
			result.add(p.getReceiver());
		}
	}

	@Override
	public void visit(AudioMessage a) {
		if (SuggestFriendsVisitor.audioToText(a).contains(user)) {
			result.add(a.getSender());
			result.add(a.getReceiver());
		}
	}

	static String audioToText(AudioMessage a) {
		return new String(a.getAudio());
	}
	
	public List<String> getResult() {
		return result;
	}
}